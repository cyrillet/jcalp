package com.valiantys.jira.plugins.createissueandlink.business;

import java.io.Serializable;
import java.util.HashMap;

/**
 * This class is the representation of the fields mapping global configuration.
 * 
 * @author Maxime
 *@since v 1.4.0
 */
public class LnioFieldsMappingConfiguration implements Serializable {

	/**
	 * serial Version UID.
	 */
	private static final long serialVersionUID = -6192266870911651633L;

	/**
	 * A list of LnioContextBean.
	 */
	private HashMap configuredContexts;

	/**
	 * default constructor.
	 */
	public LnioFieldsMappingConfiguration() {
		configuredContexts = new HashMap();
	//	configuredContexts = getHashMapfromConf();
	}
	
	public HashMap getConfiguredContexts() {
		return configuredContexts;
	}

	public void setConfiguredContexts(HashMap configuredContexts) {
		this.configuredContexts = configuredContexts;
	}

	
}
