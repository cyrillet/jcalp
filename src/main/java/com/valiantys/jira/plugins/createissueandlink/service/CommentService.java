package com.valiantys.jira.plugins.createissueandlink.service;

import com.atlassian.jira.issue.comments.Comment;

/**
 * Service used to synchronize the comments addition between 2 issues.
 * @author Maxime Cojan
 *
 */
public interface CommentService {

    /**
     * add a comment on all synchronized issues.
     * @param destinationId
     * @param aComment
     */
    public void addCommentSynchronization(Long destinationId,Comment aComment);
    
}
