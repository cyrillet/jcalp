package com.valiantys.jira.plugins.createissueandlink.conditions;

import java.util.Map;

import com.atlassian.jira.issue.IssueImpl;
import com.atlassian.jira.permission.Permission;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractJiraCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugin.PluginParseException;
import com.opensymphony.user.User;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.LinkedIssueContextManager;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChild;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChildItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

/**
 * This class represents a condition in order to display (or not) LNIO issue-operations web items
 * @author Cl�ment Capiaux
 * @since v1.5.2
 *
 */
public class LnioIsThisWebItemToBeDisplayed extends AbstractJiraCondition {


	int minMatchingMappingsForThisWebItem; /* the minimum matching mappings found to get the 
	 										* web item concerned by this condition displayed 
	 										*/
	
	private PermissionManager permissionManager;
	
	private ProjectManager projectManager;
	
	private JiraAuthenticationContext authContext;
	
	public LnioIsThisWebItemToBeDisplayed(PermissionManager permissionManager, ProjectManager projectManager, JiraAuthenticationContext authContext) {
		super();
		this.permissionManager = permissionManager;
		this.projectManager = projectManager;
		this.authContext = authContext;
	}

	public void init(Map params) throws PluginParseException {
		
		super.init(params);
		
		// retrieval of the param "minMatchingMappingsForThisWebItem" defined in the condition
		minMatchingMappingsForThisWebItem = Integer.parseInt((String) params.get("minMatchingMappingsForThisWebItem"));

	}
	
	@Override
	public boolean shouldDisplay(User user, JiraHelper jiraHelper) {
		
		boolean isThisWebItemAllowedToBeDisplayed = false;
		LinkedIssueContextManager licm = LinkedIssueContextManager.getInstance();
		
		try {
			
			// retrieval of the total number of matching mappings
			Mapping[] mappingsRanked = licm.getRankedMappingsByIssueParent((IssueImpl) jiraHelper.getContextParams().get("issue"));
			int nbMatchingMappings = mappingsRanked.length;
			
			/* if the total number of matching mappings is greater or equal to the minimum number of
			 * mappings required to get this web item displayed, the condition is satisfied, and the web
			 * is allowed to be displayed
			 */
			if (nbMatchingMappings >= minMatchingMappingsForThisWebItem) {
				
				// if the current web item is the first (which must contains the best mapping)
				if ( minMatchingMappingsForThisWebItem == 1 ) {
					
					// the best mapping must always be displayed
					isThisWebItemAllowedToBeDisplayed = true;
					
				}
				
				// if the current web item is an other than the first (not the best mapping found),
				if ( minMatchingMappingsForThisWebItem > 1 ) {
					
					/* it has to be displayed only if the configuration attribute of LNIO "multiLinkActivate"
					 * has been set to 'true'
					 */
					if ( licm.isMultiLinkActivated() ) {
						isThisWebItemAllowedToBeDisplayed = true;
					}
					
				}
				
				Mapping currentMapping = mappingsRanked[minMatchingMappingsForThisWebItem-1];
				IssueChild issueChild = currentMapping.getIssueChild();
				Long targetProjectId = getTargetIssueChildPid(jiraHelper, issueChild);
				if (targetProjectId != null && !permissionManager.hasPermission(Permissions.CREATE_ISSUE, projectManager.getProjectObj(targetProjectId), authContext.getUser())) {
					isThisWebItemAllowedToBeDisplayed = false;
				}
				
			}
			
		} catch (LinkNewIssueOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isThisWebItemAllowedToBeDisplayed;
	}

	private Long getTargetIssueChildPid(JiraHelper jiraHelper, IssueChild issueChild) {
		for (IssueChildItem item : issueChild.getIssueChildItem()) {
			if (item.getParam().getKey().equals(LinkedIssueConstants.JIRA_SUB_TASK_FLAG)) {
				return jiraHelper.getProjectObject().getId();
			}
			if (item.getParam().getKey().equals(LinkedIssueConstants.ID_PROJECT)) {
				if (item.getParam().getValue().equals(LinkedIssueConstants.CURRENT_PRJ)) {
					return jiraHelper.getProjectObject().getId();
				}else{
					return new Long(item.getParam().getValue());
				}
			}
			
		}
		return null;
	}

}
