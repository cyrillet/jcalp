package com.valiantys.jira.plugins.createissueandlink.util;

/**
 * A class of constants.
 * @author Maxime
 *@since 1.0
 */
public final class LinkedIssueConstants {
	/**
	 * Entity_Name dans la db table (propertyEntry).
	 */
	public static final String LNIO_PROPERTY_NAME = "lnil-jira-plugin.properties";
	
	public static final String  LNIO_CONFIG_XML = "lnio.config.xml";
	
	public static final String I18N = "i18n.lnio_labels";

	public static final String CONFIG_FILE_NAME = "linkedIssue-context.xml";
	
	public static final String CONFIG_SYNCHRO_FILE_NAME = "synchronizedIssues.json";
	
	public static final String TRUE = "true";
	
	public static final String ID_TYPE = "ID_TYPE";
	
	public static final String ID_STATUS = "ID_STATUS";
	
	public static final String ID_PROJECT = "ID_PROJECT";
	
	public static final String JIRA_SUB_TASK_FLAG = "JIRA_SUB_TASK_FLAG";
	
	public static final String LINK_DESCRIPTION = "LINK_DESCRIPTION";
	
	public static final String CURRENT_PRJ = "CURRENT_PRJ";
	
	public static final String OUTWARD = "outward"; 
	
	public static final String INWARD = "inward"; 
	
	public static final String ALL = "ALL"; 
	
	/**
	 * Represents the name of the jira admin groups.
	 */
	public static final String JIRA_ADMIN_GROUP = "jira-administrators";
	
	/**
	 * The create/update day punctual action name.
	 */
	public static final String ACTION_EDIT_FIELDS_MAPPING = "EditLNIOSpecificMapping.jspa?idContext=";

	/**
	 * redirection to the update success page.
	 */
	public static final String RETURN_SUCCESS = "success";

	/**
	 * redirection to the Dashboard page.
	 */
	public static final String RETURN_REDIRECT = "redirect";

	/**
	 * redirection to the Edit Mapping page.
	 */
	public static final String RETURN_UNVALIDATE = "input";

	/**
	 * redirection to the Error page.
	 */
	public static final String RETURN_ERROR = "error";
	
	/**
	 * Prefix used to generate id for html element.
	 */
	public static final String PREFIX_MAP = "map_";
	
	/**
	 * Prefix used to generate id for html element.
	 */
	public static final String PREFIX_ACTIVE = "active_";
	
	/**
	 * COnfig Path in the JIRA HOME.
	 */
	public static final String CONFIG_PATH = "plugins" + System.getProperty("file.separator") + "lnio-conf";
	
	/**
	 * constructor unaccessible.
	 */
	private LinkedIssueConstants() {
	}
}
