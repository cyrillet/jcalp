package com.valiantys.jira.plugins.createissueandlink.conditions;

import java.util.Map;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractJiraCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.plugin.PluginParseException;
import com.opensymphony.user.User;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.LinkedIssueContextManager;

public class LnioIsLinkDisplayingActivated extends AbstractJiraCondition {

	@Override
	public boolean shouldDisplay(User user, JiraHelper jiraHelper) {
		
		boolean isLinkDisplayingActivated = false;
		
		// retrieval of the contexts manager
		LinkedIssueContextManager licm = LinkedIssueContextManager.getInstance();
		
		try {
			isLinkDisplayingActivated = licm.isLinkDisplayingActivated();
		} catch (LinkNewIssueOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return isLinkDisplayingActivated;
	}

	public void init(Map params) throws PluginParseException {

		super.init(params);

	}
	
}
