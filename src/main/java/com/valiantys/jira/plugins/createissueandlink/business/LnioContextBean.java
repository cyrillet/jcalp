package com.valiantys.jira.plugins.createissueandlink.business;

import java.io.Serializable;
import java.util.List;


/**
 * This class is the representation of a context (mapping) defined in the
 * linkedIssue-context.xml.
 * 
 * @author Maxime
 *@since v 1.4.0
 */
public class LnioContextBean implements Serializable, Comparable {

	/**
	 * serial Version UID.
	 */
	private static final long serialVersionUID = -6710959135124878195L;

	/**
	 * The context id defined in the xml file.
	 */
	private String idContext;

	/**
	 * The html link generated to go to the fields mapping edit screen of this
	 * context.
	 */
	private String link;

	/**
	 * a label to display in the list screen.
	 */
	private String label;

	/**
	 * A list of LnioMappingOfFields for this context. 
	 */
	private List listOfFieldsMapping;
	
	/**
	 * a name to display in the list screen.
	 */
	private String name;
	
			
	private String parentIssueIdType;
	private String parentIssueIdProject;

	private String childIssueIdType;
	private String childIssueIdProject;

	
	public String getIdContext() {
		return idContext;
	}

	public void setIdContext(String idContext) {
		this.idContext = idContext;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getParentIssueIdType() {
		return parentIssueIdType;
	}

	public void setParentIssueIdType(String parentIssueIdType) {
		this.parentIssueIdType = parentIssueIdType;
	}

	public String getParentIssueIdProject() {
		return parentIssueIdProject;
	}

	public void setParentIssueIdProject(String parentIssueIdProject) {
		this.parentIssueIdProject = parentIssueIdProject;
	}

	public String getChildIssueIdType() {
		return childIssueIdType;
	}

	public void setChildIssueIdType(String childIssueIdType) {
		this.childIssueIdType = childIssueIdType;
	}

	public String getChildIssueIdProject() {
		return childIssueIdProject;
	}

	public void setChildIssueIdProject(String childIssueIdProject) {
		this.childIssueIdProject = childIssueIdProject;
	}

	public List getListOfFieldsMapping() {
		return listOfFieldsMapping;
	}

	public void setListOfFieldsMapping(List listOfFieldsMapping) {
		this.listOfFieldsMapping = listOfFieldsMapping;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(Object arg0) {
//		Integer i1 = new Integer(idContext);
//		Integer i2 = new Integer(((LnioContextBean)arg0).getIdContext());
		String i1 = idContext;
		String i2 = ((LnioContextBean)arg0).getIdContext();
		return i1.compareTo(i2);
	}
}
