package com.valiantys.jira.plugins.createissueandlink.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericValue;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.JiraException;
import com.atlassian.jira.event.issue.IssueEventSource;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.UpdateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.impl.MultiSelectCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.OutlookDate;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.opensymphony.user.Group;
import com.opensymphony.user.User;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.LinkedIssueContextManager;
import com.valiantys.jira.plugins.createissueandlink.business.LnioContextBean;
import com.valiantys.jira.plugins.createissueandlink.business.LnioFieldsMappingConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.LnioMappingOfFieldBean;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.manager.LnioConfigurationManager;
import com.valiantys.jira.plugins.createissueandlink.util.FieldsMappingExecutionContext;
import com.valiantys.jira.plugins.createissueandlink.util.SynchronizationType;

/**
 * Default implementation of the FieldsMappingService for syncrhonization.
 * 
 * @author Maxime Cojan
 * 
 */
public class FieldsMappingServiceDefaultImpl implements FieldsMappingService {

    /**
     * Logger.
     */
    private static final Logger LOG = Logger.getLogger(FieldsMappingServiceDefaultImpl.class);

    /**
     * Issue manager
     */
    private final IssueManager issueManager;

    /**
     * Plugin configuration manager : mapping and synchro
     */
    private final LnioConfigurationManager lnioConfigManager;

    /**
     * manger of fields
     */
    private final FieldManager fieldManager;

    /**
     * Manager of custom fields.
     */
    private final CustomFieldManager cfManager;

    /**
     * Issue updater for the change history management.
     */
    private final IssueUpdater issueUpdater;

    /**
     * Authentication context.
     */
    private final JiraAuthenticationContext jiraAuthenticationContext;

    private final IssueIndexManager indexManager;

    /**
     * constructor
     * 
     * @param issueManager
     * @param lnioConfigManager
     * @param fieldManager
     * @param cfManager
     * @param jiraAuthenticationContext
     * @param issueUpdater
     */
    public FieldsMappingServiceDefaultImpl(final IssueManager issueManager, final LnioConfigurationManager lnioConfigManager, final FieldManager fieldManager, final CustomFieldManager cfManager,
	    final JiraAuthenticationContext jiraAuthenticationContext, final IssueUpdater issueUpdater, final IssueIndexManager indexManager) {
	this.issueManager = issueManager;
	this.lnioConfigManager = lnioConfigManager;
	this.fieldManager = fieldManager;
	this.cfManager = cfManager;
	this.jiraAuthenticationContext = jiraAuthenticationContext;
	this.issueUpdater = issueUpdater;
	this.indexManager = indexManager;
	
    }

    /**
     * {@inheritDoc}
     */
    public boolean doSynchronization(String mappingId, Long destinationId, Long originId, boolean reverseMapping) {
	boolean hasIssueBeenUpdated = false;
	LOG.debug("synchro fields...");
	LinkedIssueContextManager licm = LinkedIssueContextManager.getInstance();
	Mapping mapping = null;
	try {
	    mapping = licm.getMappingById(mappingId);
	} catch (LinkNewIssueOperationException e) {
	    e.printStackTrace();
	}

	
	MutableIssue destinationIssue = issueManager.getIssueObject(destinationId);
	Issue originIssue = issueManager.getIssueObject(originId);

	Collection<ChangeItemBean> changeItemBeans = mapFieldsWithInheritValues(destinationIssue, originIssue, mapping, FieldsMappingExecutionContext.SYNCHRONIZATION, reverseMapping);
	IssueUpdateBean issueUpdateBean = new IssueUpdateBean(destinationIssue.getGenericValue(), destinationIssue.getGenericValue(), EventType.ISSUE_UPDATED_ID, jiraAuthenticationContext.getUser());
	
	if(changeItemBeans!=null && changeItemBeans.size()>0){
	    issueUpdateBean.setChangeItems(changeItemBeans);
	    hasIssueBeenUpdated = true;
	    try {
		    issueManager.updateIssue(jiraAuthenticationContext.getUser(), destinationIssue, EventDispatchOption.ISSUE_UPDATED, true);
		} catch (UpdateException e) {
		    e.printStackTrace();
		}
	    destinationIssue.store();
	    try {
		indexManager.reIndex(destinationIssue);
	    } catch (IndexException e) {
		e.printStackTrace();
	    }
	}
	
	
	LOG.debug("Synchro is done between Origin : " + originIssue.getKey() + " Destination : " + destinationIssue.getKey() + " Mapping : " + mappingId);
	return hasIssueBeenUpdated;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<ChangeItemBean> mapFieldsWithInheritValues(MutableIssue destinationIssue, Issue origineIssue, Mapping mapping, FieldsMappingExecutionContext executionContext,   boolean reverseMapping) {

	LnioMappingOfFieldBean fieldMappingTemp;
	String sourceFieldId = "";
	String destinationFieldId = "";
	Boolean isReporterMapped = false;
	Boolean isSummaryMapped = false;

	LnioFieldsMappingConfiguration lnioFieldsConfig = null;
	
	Collection<ChangeItemBean> changeItemBeans = null;
	
	try {
	    lnioFieldsConfig = lnioConfigManager.getLnioFieldsMappingConfigurationObject();
	} catch (LinkNewIssueOperationException e) {
	    e.printStackTrace();
	    LOG.error("Failed to load LNIO configuration");
	}

	LnioContextBean lcb = (LnioContextBean) lnioFieldsConfig.getConfiguredContexts().get(String.valueOf(mapping.getId()));
	List fieldsMapping = lcb.getListOfFieldsMapping();

	if (fieldsMapping != null) {

	    Iterator it = fieldsMapping.iterator();

	    ChangeItemBean aChangeItemBean = null;

	    while (it.hasNext()) {

		fieldMappingTemp = (LnioMappingOfFieldBean) it.next();

		if (!reverseMapping) {
		    sourceFieldId = fieldMappingTemp.getOriginFieldId();
		    destinationFieldId = fieldMappingTemp.getDestinationFieldId();
		} else {
		    sourceFieldId = fieldMappingTemp.getDestinationFieldId();
		    destinationFieldId = fieldMappingTemp.getOriginFieldId();
		}

		SynchronizationType typeOfFieldsSynchro = getFieldSynchroType(destinationFieldId, sourceFieldId);
		LOG.debug("The type of field's synchronization  is : " + typeOfFieldsSynchro.toString());
		LOG.debug("Destination KEY : " + destinationIssue.getKey());
		LOG.debug("Origin KEY : " + origineIssue.getKey());

		switch (typeOfFieldsSynchro) {

		case CF_TO_CF:
		    aChangeItemBean = synchroCFToCF(sourceFieldId, destinationFieldId, origineIssue, destinationIssue);
		    break;
		case FIELD_TO_CF:
		    aChangeItemBean = synchroFieldToCF(sourceFieldId, destinationFieldId, origineIssue, destinationIssue);
		    break;
		case CF_TO_FIELD:
		    aChangeItemBean = synchroCFToField(sourceFieldId, destinationFieldId, origineIssue, destinationIssue, isSummaryMapped);
		    break;
		case FIELD_TO_FIELD:
		    aChangeItemBean = synchroFieldToField(sourceFieldId, destinationFieldId, origineIssue, destinationIssue, isReporterMapped, isSummaryMapped);
		    break;
		}
		if(aChangeItemBean != null){
		    if (changeItemBeans == null) {
			changeItemBeans = new ArrayList<ChangeItemBean>();
		    }
		    changeItemBeans.add(aChangeItemBean);
		}
	    }
	    
	    // Issue types synchro
	    aChangeItemBean = synchroIssueType(origineIssue, destinationIssue);
		if(aChangeItemBean != null){
		    if (changeItemBeans == null) {
			changeItemBeans = new ArrayList<ChangeItemBean>();
		    }
		    changeItemBeans.add(aChangeItemBean);
		}
	}

	if (executionContext.equals(FieldsMappingExecutionContext.CREATION)) {

	    if (!isReporterMapped) {
		destinationIssue.setReporter(origineIssue.getReporter());
	    }

	    if (!isSummaryMapped) {
		I18nHelper i18n = ComponentManager.getInstance().getJiraAuthenticationContext().getI18nHelper();
		StringBuffer sb = new StringBuffer();
		sb.append(i18n.getText("summary.prefix"));
		sb.append(" : ");
		sb.append(origineIssue.getSummary());
		destinationIssue.setSummary(sb.toString());
	    }
	}
	return changeItemBeans;

    }

    private ChangeItemBean synchroIssueType(Issue origineIssue, MutableIssue destinationIssue) {
    	ChangeItemBean aChangeItemBean = null;
    	LOG.debug("Synchro issue types");
    	
    	// ISSUE TYPE
	    String from = String.valueOf(destinationIssue.getIssueTypeObject());
		destinationIssue.setIssueTypeId(origineIssue.getIssueTypeObject().getId());
		String to = String.valueOf(origineIssue.getIssueTypeObject());
		
		if (to != null) {
		    boolean toUpdate = true;
		    if (from != null) {
			if (from.toString().equals(to.toString())) {
			    toUpdate = false;
			}
		    }
		    if (toUpdate) {
			aChangeItemBean = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, IssueFieldConstants.ISSUE_TYPE, from, to);
		    }
		}
		
		return aChangeItemBean;
	}

	/**
     * get the type of field synchronization : 4 type available defined by the
     * SynchronizationType enumeration.
     * 
     * @param destinationFieldId
     * @param sourceFieldId
     * @return
     */
    private SynchronizationType getFieldSynchroType(String destinationFieldId, String sourceFieldId) {

	LOG.debug("sourceFieldId : " + sourceFieldId);
	LOG.debug("destinationFieldId : " + destinationFieldId);
	if (destinationFieldId.startsWith("customfield_")) {
	    if (sourceFieldId.startsWith("customfield_")) {
		return SynchronizationType.CF_TO_CF;
	    } else {
		return SynchronizationType.FIELD_TO_CF;
	    }
	} else {
	    if (sourceFieldId.startsWith("customfield_")) {
		return SynchronizationType.CF_TO_FIELD;
	    } else {
		return SynchronizationType.FIELD_TO_FIELD;
	    }
	}
    }

    /**
     * Synchronize a Customfield to another Customfield
     * 
     * @param sourceFieldId
     * @param destinationFieldId
     * @param origineIssue
     * @param destinationIssue
     * @return
     */
    private ChangeItemBean synchroCFToCF(String sourceFieldId, String destinationFieldId, Issue origineIssue, MutableIssue destinationIssue) {

	Object newValue = null;
	ChangeItemBean aChangeItemBean = null;

	LOG.debug("sourceFieldId : " + sourceFieldId);
	LOG.debug("destinationFieldId : " + destinationFieldId);

	if (sourceFieldId.compareTo(destinationFieldId) != 0) {

	    OutlookDateManager outlookDateManager = ComponentManager.getInstance().getOutlookDateManager();

	    Object cfValue = origineIssue.getCustomFieldValue((CustomField) fieldManager.getCustomField(sourceFieldId));
	    if (cfValue != null) {
		LOG.debug("cfValue : " + cfValue + " - " + cfValue.getClass().getName());

		if (cfValue instanceof User) {
		    newValue = ((User) cfValue).getName();
		} else if (cfValue instanceof Group) {
		    newValue = ((Group) cfValue).getName();
		} else if (cfValue instanceof Project) {
		    newValue = ((Project) cfValue).getName();
		} else if (cfValue instanceof ProjectRole) {
		    newValue = ((ProjectRole) cfValue).getName();
		} else if (cfValue instanceof String) {
		    newValue = cfValue.toString();
		} else if (cfValue instanceof Date) {
		    OutlookDate outlookDate = outlookDateManager.getOutlookDate(jiraAuthenticationContext.getLocale());
		    newValue = outlookDate.format((Date) cfValue);
		} else if (cfValue instanceof Timestamp) {
		    OutlookDate outlookDate = outlookDateManager.getOutlookDate(jiraAuthenticationContext.getLocale());
		    newValue = outlookDate.format(new Date(((Timestamp) cfValue).getTime()));
		} else if (cfValue instanceof Double) {
		    newValue = new Float(((Double) cfValue).floatValue()).toString();

		} else {
		    newValue = cfValue;
		}

		List customFieldParams = new ArrayList();

		if (newValue instanceof ArrayList) {

		    Object objectTmp = null;

		    ArrayList l = (ArrayList) newValue;
		    for (int i = 0; i < l.size(); i++) {
			if (l.get(i) instanceof User) {
			    objectTmp = ((User) cfValue).getName();
			} else if (l.get(i) instanceof Group) {
			    objectTmp = ((Group) cfValue).getName();
			} else if (l.get(i) instanceof Project) {
			    objectTmp = ((Project) cfValue).getName();
			} else if (l.get(i) instanceof ProjectRole) {
			    objectTmp = ((ProjectRole) cfValue).getName();
			} else if (l.get(i) instanceof String) {
			    objectTmp = cfValue.toString();
			} else if (l.get(i) instanceof Date) {
			    OutlookDate outlookDate = outlookDateManager.getOutlookDate(jiraAuthenticationContext.getLocale());
			    objectTmp = outlookDate.format((Date) l.get(i));
			} else if (l.get(i) instanceof Timestamp) {
			    OutlookDate outlookDate = outlookDateManager.getOutlookDate(jiraAuthenticationContext.getLocale());
			    objectTmp = outlookDate.format(new Date(((Timestamp) l.get(i)).getTime()));
			} else if (l.get(i) instanceof Double) {
			    objectTmp = new Float(((Double) l.get(i)).floatValue()).toString();

			} else {
			    objectTmp = l.get(i);
			}
			customFieldParams.add(objectTmp);
		    }

		    destinationIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), customFieldParams);

		} else {

		    // if destination field is a multi select list
		    if (cfManager.getCustomFieldObject(destinationFieldId).getCustomFieldType() instanceof MultiSelectCFType) {
			List multiSelectDestinationValue = new ArrayList();
			multiSelectDestinationValue.add(origineIssue.getCustomFieldValue(cfManager.getCustomFieldObject(sourceFieldId)).toString());
			destinationIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), multiSelectDestinationValue);
		    } else {
			destinationIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), origineIssue.getCustomFieldValue(cfManager.getCustomFieldObject(sourceFieldId)));
		    }
		}
	    }
	} else {

	    Object to = origineIssue.getCustomFieldValue(cfManager.getCustomFieldObject(sourceFieldId));
	    Object from = destinationIssue.getCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId));
	    LOG.debug("destinationIssue" + destinationIssue + " - destinationFieldId : " + (cfManager.getCustomFieldObject(destinationFieldId)));
	    LOG.debug("to : " + to + " - from : " + from);

	    if (from == null && to==null) {
		return null;
	    }else{
		destinationIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), to);
		boolean toUpdate = true;
		if (from != null && to!=null) {
		    if (from.toString().equals(to.toString())) {
			toUpdate = false;
		    }
		}
		if (toUpdate) {
		    aChangeItemBean = new ChangeItemBean(ChangeItemBean.CUSTOM_FIELD, cfManager.getCustomFieldObject(destinationFieldId).getName(), String.valueOf(from), String.valueOf(to));
		}
	    }
	}
	return aChangeItemBean;
    }

    /**
     * Synchronize a Customfield to another field
     * 
     * @param sourceFieldId
     * @param destinationFieldId
     * @param origineIssue
     * @param destinationIssue
     * @param isSummaryMapped
     * @return
     */
    private ChangeItemBean synchroCFToField(String sourceFieldId, String destinationFieldId, Issue origineIssue, MutableIssue destinationIssue, Boolean isSummaryMapped) {
	ChangeItemBean aChangeItemBean = null;

	Object sourceCustomFieldValue = origineIssue.getCustomFieldValue(cfManager.getCustomFieldObject(sourceFieldId));
	Object from = null;

	try {
	    if (destinationFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
		from = destinationIssue.getEnvironment();
		destinationIssue.setEnvironment((String) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
		from = destinationIssue.getStatusObject().getName();
		destinationIssue.setStatus((GenericValue) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		from = destinationIssue.getResolutionDate();
		destinationIssue.setResolutionDate((Timestamp) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		from = destinationIssue.getDueDate();
		destinationIssue.setDueDate((Timestamp) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
		from = destinationIssue.getFixVersions();
		destinationIssue.setFixVersions((Collection<Version>) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
		from = destinationIssue.getDescription();
		destinationIssue.setDescription((String) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
		from = destinationIssue.getPriorityObject().getName();
		destinationIssue.setPriority((GenericValue) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
		from = destinationIssue.getComponentObjects();
		destinationIssue.setComponents((Collection<GenericValue>) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
		from = destinationIssue.getSecurityLevel();
		destinationIssue.setSecurityLevel((GenericValue) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
		from = destinationIssue.getResolutionObject().getName();
		destinationIssue.setResolution((GenericValue) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
		from = destinationIssue.getAffectedVersions();
		destinationIssue.setAffectedVersions((Collection<Version>) sourceCustomFieldValue);
	    }
	    if (destinationFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
		from = destinationIssue.getSummary();
		destinationIssue.setSummary(sourceCustomFieldValue.toString());
		isSummaryMapped = true;
	    }

	    if (sourceCustomFieldValue != null) {
		boolean toUpdate = true;
		if (from != null) {
		    if (from.toString().equals(sourceCustomFieldValue.toString())) {
			toUpdate = false;
		    }
		    if (toUpdate) {
			aChangeItemBean = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, destinationFieldId, from.toString(), sourceCustomFieldValue.toString());
		    }
		}
	    }
	} catch (NullPointerException e) {
	    LOG.debug("For Source field : " + sourceFieldId + " and the origine issue : " + origineIssue + " the field : " + destinationFieldId + " will not be fill.");
	}

	return aChangeItemBean;
    }

    /**
     * Synchronize a field to another Customfield
     * 
     * @param sourceFieldId
     * @param destinationFieldId
     * @param origineIssue
     * @param destinationIssue
     * @return
     */
    private ChangeItemBean synchroFieldToCF(String sourceFieldId, String destinationFieldId, Issue origineIssue, MutableIssue destinationIssue) {
	ChangeItemBean aChangeItemBean = null;

	Object to = null;

	Object from = null;
	try {
	    from = destinationIssue.getCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId));
	    if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
		to = origineIssue.getEnvironment();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
		to = origineIssue.getStatusObject().getName();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		to = origineIssue.getResolutionDate();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		to = origineIssue.getDueDate();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
		to = origineIssue.getFixVersions();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
		to = origineIssue.getDescription();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
		to = origineIssue.getPriority();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
		to = origineIssue.getComponents();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
		to = origineIssue.getSecurityLevel();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
		to = origineIssue.getResolution();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
		to = origineIssue.getAffectedVersions();
	    }
	    if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
		to = origineIssue.getSummary();
	    }

	    destinationIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), to);
	    issueManager.updateIssue(jiraAuthenticationContext.getUser(), destinationIssue, EventDispatchOption.DO_NOT_DISPATCH, false);
	} catch (NullPointerException e) {
	    LOG.debug("For Source field : " + sourceFieldId + " and the origine issue : " + origineIssue + " the field : " + destinationFieldId + " will not be fill.");
	} catch (UpdateException e) {
	    e.printStackTrace();
	}

	if (to != null) {
	    boolean toUpdate = true;
	    if (from != null) {
		if (from.toString().equals(to.toString())) {
		    toUpdate = false;
		}
	    }
	    if (toUpdate) {
		aChangeItemBean = new ChangeItemBean(ChangeItemBean.CUSTOM_FIELD, cfManager.getCustomFieldObject(destinationFieldId).getName(), from.toString(), to.toString());
	    }
	}
	return aChangeItemBean;
    }

    /**
     * Synchronize a field to another field
     * 
     * @param sourceFieldId
     * @param destinationFieldId
     * @param origineIssue
     * @param destinationIssue
     * @param isReporterMapped
     * @param isSummaryMapped
     * @return
     */
    private ChangeItemBean synchroFieldToField(String sourceFieldId, String destinationFieldId, Issue origineIssue, MutableIssue destinationIssue, Boolean isReporterMapped, Boolean isSummaryMapped) {
	ChangeItemBean aChangeItemBean = null;
	LOG.debug("Syncrho Field to field");

	String from = "";
	String to = "";

	try {
	    // REPORTER
	    if (destinationFieldId.compareTo(IssueFieldConstants.REPORTER) == 0) {
		from = destinationIssue.getReporter().getFullName();
		if (sourceFieldId.compareTo(IssueFieldConstants.REPORTER) == 0) {
		    destinationIssue.setReporter(origineIssue.getReporter());
		    to = origineIssue.getReporter().getFullName();
		    isReporterMapped = true;
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.ASSIGNEE) == 0) {
		    destinationIssue.setReporter(origineIssue.getAssignee());
		    to = origineIssue.getAssignee().getFullName();
		    isReporterMapped = true;
		}
	    }

	    // ASSIGNEE
	    if (destinationFieldId.compareTo(IssueFieldConstants.ASSIGNEE) == 0) {
		from = destinationIssue.getAssignee().getFullName();

		if (sourceFieldId.compareTo(IssueFieldConstants.REPORTER) == 0) {
		    destinationIssue.setReporter(origineIssue.getReporter());
		    to = origineIssue.getReporter().getFullName();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.ASSIGNEE) == 0) {
		    destinationIssue.setReporter(origineIssue.getAssignee());
		    to = origineIssue.getAssignee().getFullName();
		}
	    }

	    // SUMMARY
	    if (destinationFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
		from = destinationIssue.getSummary();
		if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
		    destinationIssue.setSummary(origineIssue.getEnvironment());
		    to = origineIssue.getEnvironment();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		    destinationIssue.setSummary(String.valueOf(origineIssue.getResolutionDate()));
		    to = String.valueOf(origineIssue.getResolutionDate());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		    destinationIssue.setSummary(String.valueOf(origineIssue.getDueDate()));
		    to = String.valueOf(origineIssue.getDueDate());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
		    destinationIssue.setSummary(origineIssue.getDescription());
		    to = origineIssue.getDescription();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
		    destinationIssue.setSummary(origineIssue.getSummary());
		    to = origineIssue.getSummary();
		}
		isSummaryMapped = true;
	    }

	    // ENVIRONMENT
	    if (destinationFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {

		from = destinationIssue.getEnvironment();
		if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getEnvironment());
		    to = origineIssue.getEnvironment();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getStatusObject().toString());
		    to = origineIssue.getStatusObject().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getResolutionDate().toString());
		    to = origineIssue.getResolutionDate().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getDueDate().toString());
		    to = origineIssue.getDueDate().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getFixVersions().toString());
		    to = origineIssue.getFixVersions().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getDescription());
		    to = origineIssue.getDescription();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getPriority().toString());
		    to = origineIssue.getPriority().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getComponents().toString());
		    to = origineIssue.getComponents().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getSecurityLevel().toString());
		    to = origineIssue.getSecurityLevel().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getResolution().toString());
		    to = origineIssue.getResolution().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getAffectedVersions().toString());
		    to = origineIssue.getAffectedVersions().toString();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
		    destinationIssue.setEnvironment(origineIssue.getSummary());
		    to = origineIssue.getSummary();
		}
	    }

	    // STATUS
	    if (destinationFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
		from = destinationIssue.getStatusObject().getId();
		if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
		    destinationIssue.setStatusId(origineIssue.getEnvironment());
		    to = origineIssue.getEnvironment();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
		    destinationIssue.setStatusId(origineIssue.getStatusObject().getId());
		    to = origineIssue.getStatusObject().getId();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		    destinationIssue.setStatusId(String.valueOf(origineIssue.getResolutionDate()));
		    to = String.valueOf(origineIssue.getResolutionDate());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		    destinationIssue.setStatusId(String.valueOf(origineIssue.getDueDate()));
		    to = String.valueOf(origineIssue.getDueDate());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
		    destinationIssue.setStatusId(String.valueOf(origineIssue.getFixVersions()));
		    to = String.valueOf(origineIssue.getFixVersions());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
		    destinationIssue.setStatusId(origineIssue.getDescription());
		    to = origineIssue.getDescription();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
		    destinationIssue.setStatusId(String.valueOf(origineIssue.getPriorityObject()));
		    to = String.valueOf(origineIssue.getPriorityObject());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
		    destinationIssue.setStatusId(String.valueOf(origineIssue.getComponents()));
		    to = String.valueOf(origineIssue.getComponents());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
		    destinationIssue.setStatusId(String.valueOf(origineIssue.getSecurityLevel()));
		    to = String.valueOf(origineIssue.getSecurityLevel());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
		    destinationIssue.setStatusId(String.valueOf(origineIssue.getResolution()));
		    to = String.valueOf(origineIssue.getResolution());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
		    destinationIssue.setStatusId(String.valueOf(origineIssue.getAffectedVersions()));
		    to = String.valueOf(origineIssue.getAffectedVersions());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
		    destinationIssue.setStatusId(origineIssue.getSummary());
		    to = origineIssue.getSummary();
		}
	    }

	    // RESOLUTION DATE
	    if (destinationFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		from = String.valueOf(destinationIssue.getResolutionDate());
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		    destinationIssue.setResolutionDate(origineIssue.getResolutionDate());
		    to = String.valueOf(origineIssue.getResolutionDate());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		    destinationIssue.setResolutionDate(origineIssue.getDueDate());
		    to = String.valueOf(origineIssue.getDueDate());
		}
	    }

	    // DUE DATE
	    if (destinationFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		from = String.valueOf(destinationIssue.getDueDate());
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		    destinationIssue.setDueDate(origineIssue.getResolutionDate());
		    to = String.valueOf(origineIssue.getResolutionDate());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		    destinationIssue.setDueDate(origineIssue.getDueDate());
		    to = String.valueOf(origineIssue.getDueDate());
		}
	    }

	    // FIX VERSIONS
	    if (destinationFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
		from = String.valueOf(destinationIssue.getFixVersions());
		if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
		    destinationIssue.setFixVersions(origineIssue.getFixVersions());
		    to = String.valueOf(origineIssue.getFixVersions());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
		    destinationIssue.setFixVersions(origineIssue.getAffectedVersions());
		    to = String.valueOf(origineIssue.getFixVersions());
		}
	    }

	    // DESCRIPTION
	    if (destinationFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
		from = destinationIssue.getDescription();
		LOG.debug("cas descript to map in dest");

		if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
		    destinationIssue.setDescription(origineIssue.getEnvironment());
		    to = origineIssue.getEnvironment();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
		    destinationIssue.setDescription(origineIssue.getStatusObject().toString());
		    to = String.valueOf(origineIssue.getStatusObject());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
		    destinationIssue.setDescription(origineIssue.getResolutionDate().toString());
		    to = String.valueOf(origineIssue.getResolutionDate());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
		    destinationIssue.setDescription(origineIssue.getDueDate().toString());
		    to = String.valueOf(origineIssue.getDueDate());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
		    destinationIssue.setDescription(origineIssue.getFixVersions().toString());
		    to = String.valueOf(origineIssue.getFixVersions());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
		    destinationIssue.setDescription(origineIssue.getDescription());
		    to = origineIssue.getDescription();
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
		    destinationIssue.setDescription(origineIssue.getPriorityObject().toString());
		    to = String.valueOf(origineIssue.getPriorityObject());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
		    destinationIssue.setDescription(origineIssue.getComponents().toString());
		    to = String.valueOf(origineIssue.getComponents());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
		    destinationIssue.setDescription(origineIssue.getSecurityLevel().toString());
		    to = String.valueOf(origineIssue.getSecurityLevel());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
		    destinationIssue.setDescription(origineIssue.getResolution().toString());
		    to = String.valueOf(origineIssue.getResolution());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
		    destinationIssue.setDescription(origineIssue.getAffectedVersions().toString());
		    to = String.valueOf(origineIssue.getAffectedVersions());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
		    destinationIssue.setDescription(origineIssue.getSummary());
		    to = origineIssue.getSummary();
		}
	    }

	    // PRIORITY
	    if (destinationFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
		if(destinationIssue.getPriorityObject() != null){
		    from = destinationIssue.getPriorityObject().getId();
		}else{
		    from = "";
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0 && origineIssue.getPriority()!= null) {
		    destinationIssue.setPriority(origineIssue.getPriority());
		    to = origineIssue.getPriorityObject().getId();
		}
	    }

	    // COMPONENTS
	    if (destinationFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
		from = String.valueOf(destinationIssue.getComponents());
		if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
		    destinationIssue.setComponents(origineIssue.getComponents());
		    to = String.valueOf(origineIssue.getComponents());
		}
	    }

	    // SECURITY
	    if (destinationFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
		from = String.valueOf(destinationIssue.getSecurityLevel());
		if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
		    destinationIssue.setSecurityLevel(origineIssue.getSecurityLevel());
		    to = String.valueOf(origineIssue.getSecurityLevel());
		}
	    }

	    // RESOLUTION
	    if (destinationFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
		from = String.valueOf(destinationIssue.getResolutionObject());
		if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
		    destinationIssue.setResolution(origineIssue.getResolution());
		    to = String.valueOf(origineIssue.getResolutionObject());
		}
	    }

	    // AFFECTED VERSIONS
	    if (destinationFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
		from = String.valueOf(destinationIssue.getAffectedVersions());
		if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
		    destinationIssue.setAffectedVersions(origineIssue.getFixVersions());
		    to = String.valueOf(origineIssue.getFixVersions());
		}
		if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
		    destinationIssue.setAffectedVersions(origineIssue.getAffectedVersions());
		    to = String.valueOf(origineIssue.getAffectedVersions());
		}
	    }

	} catch (NullPointerException e) {
	    LOG.debug("For Source field : " + sourceFieldId + " and the origine issue : " + origineIssue + " the field : " + destinationFieldId + " will not be fill.");
	}

	if (to != null) {
	    boolean toUpdate = true;
	    if (from != null) {
		if (from.toString().equals(to.toString())) {
		    toUpdate = false;
		}
	    }
	    if (toUpdate) {
		aChangeItemBean = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, destinationFieldId, from, to);
	    }
	}

	return aChangeItemBean;
    }


}