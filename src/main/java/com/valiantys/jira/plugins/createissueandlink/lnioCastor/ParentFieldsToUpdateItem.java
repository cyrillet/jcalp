/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3rc1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.valiantys.jira.plugins.createissueandlink.lnioCastor;

/**
 * Class ParentFieldsToUpdateItem.
 * 
 * @version $Revision$ $Date$
 */
public class ParentFieldsToUpdateItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _field.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.Field _field;


      //----------------/
     //- Constructors -/
    //----------------/

    public ParentFieldsToUpdateItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'field'.
     * 
     * @return the value of field 'Field'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.Field getField(
    ) {
        return this._field;
    }

    /**
     * Sets the value of field 'field'.
     * 
     * @param field the value of field 'field'.
     */
    public void setField(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.Field field) {
        this._field = field;
    }

}
