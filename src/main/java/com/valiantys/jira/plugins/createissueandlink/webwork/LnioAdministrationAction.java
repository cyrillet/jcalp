package com.valiantys.jira.plugins.createissueandlink.webwork;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Category;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.opensymphony.user.User;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.LinkedIssueContextManager;
import com.valiantys.jira.plugins.createissueandlink.business.LnioContextBean;
import com.valiantys.jira.plugins.createissueandlink.business.LnioFieldsMappingConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.LnioMappingOfFieldBean;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChild;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChildItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParent;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueParentItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.MappingsItem;
import com.valiantys.jira.plugins.createissueandlink.manager.ILnioConfigurationManager;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;
import com.valiantys.jira.plugins.createissueandlink.util.LnioUtils;

/**
 * Web actions to edit and update the LNIO PLUGIN configuration.<br>
 * You can add specific fields mapping for a context.<br>
 * 
 * @author Maxime
 * @since v 1.4.0
 */
public class LnioAdministrationAction extends JiraWebActionSupport {

	/**
	 * The serial ID.
	 */
	private static final long serialVersionUID = 351916174672386910L;

	/**
	 * Logger.
	 */
	private static final Category LOG = Category
			.getInstance(LnioAdministrationAction.class);

	/**
	 * Authentication context.
	 */
	private final JiraAuthenticationContext authenticationContext;

	/**
	 * Configuration Manager of LNIO.
	 */
	private final ILnioConfigurationManager lnioConfigManager;

	/**
	 * The LNIO configuration object.
	 */
	private LnioFieldsMappingConfiguration lnioConfig = null;

	/**
	 * The jira instance project manager.
	 */
	private final ProjectManager projectManager;

	/**
	 * The jira instance issue manager.
	 */
	private final IssueManager issueManager;

	/**
	 * The jira instance project manager.
	 */
	private final FieldConfigSchemeManager fieldConfigSchemeManager;

	/**
	 * Operation states result for operations.<br>
	 * Used to display action result to the user.
	 */
	private String resultOperation = null;

	/**
	 * variable used to execute action on one particular context.
	 */
	private String idContext;

	/**
	 * Bean used to get internationalized texts.
	 */
	private I18nHelper i18n;

	/**
	 * List of fields id which are not mapped.
	 */
	private final String[] FIELDS_KEYS = { "created", "issuetype", "updated",
			"issuekey", "votes", "workratio", "thumbnail", "project",
			"issuelinks" };
	

	/**
	 * Constructor.
	 * 
	 * @param context
	 *            instance of the JiraAuthenticationContext object.
	 * @param lnioConfManager
	 *            instance of the ISLATimeConfigManager object.
	 * @param fieldConfigSchemeMng
	 *            instance of the FieldConfigSchemeMng object.
	 * @param projectMng
	 *            instance of the ProjectManager object.
	 * @param issueMng
	 *            instance of the IssueManager object.
	 */
	public LnioAdministrationAction(final JiraAuthenticationContext context,
			final ILnioConfigurationManager lnioConfManager,
			final FieldConfigSchemeManager fieldConfigSchemeMng,
			final ProjectManager projectMng, final IssueManager issueMng) {

		this.authenticationContext = context;
		this.lnioConfigManager = lnioConfManager;
		this.projectManager = projectMng;
		this.fieldConfigSchemeManager = fieldConfigSchemeMng;
		this.issueManager = issueMng;

		//this.i18n = authenticationContext.getI18nBean(LinkedIssueConstants.I18N);
		this.i18n = ComponentManager.getInstance().getJiraAuthenticationContext().getI18nHelper();
	}

	/**
	 * This method is call when the user want to edit the LNIO configuration.<br>
	 * It is the first level of the edit configuration.<br>
	 * Possible next page name are :<br>
	 * -success : if all action succeed.<br>
	 * -error : if there is error.<br>
	 * -redirect : if user is not allowed.<br>
	 * Page name represents real page define in the atlassian-plugin.xml file.
	 * 
	 * @return the next page name.
	 */
	public final String doEditConfig() {
		String returnPage = LinkedIssueConstants.RETURN_REDIRECT;
		// User Acces rights check.
		if (!isUserNotAllowed()) {
			// Retrieve the configuration
			getLNIOConf();
			returnPage = LinkedIssueConstants.RETURN_SUCCESS;
		}
		return returnPage;
	}

	/**
	 * This method is call when the user want to edit a specific mapping of
	 * fields in the configuration.<br>
	 * Possible next page name are :<br>
	 * -success : if all action succeed.<br>
	 * -error : if there is error.<br>
	 * -redirect : if user is not allowed.<br>
	 * Page name represents real page define in the atlassian-plugin.xml file.
	 * 
	 * @return the next page name.
	 */
	public final String doEditSpecificMapping() {
		String returnPage = LinkedIssueConstants.RETURN_REDIRECT;
		if (!isUserNotAllowed()) {
			returnPage = LinkedIssueConstants.RETURN_SUCCESS;
		}
		return returnPage;
	}

	/**
	 * This method is call when the user want to save the new mapping.<br>
	 * Possible next page name are :<br>
	 * -success : if all action succeed.<br>
	 * -error : if there is error.<br>
	 * -redirect : if user is not allowed.<br>
	 * Page name represents real page define in the atlassian-plugin.xml file.
	 * 
	 * @return the next page name.
	 */
	public final String doUpdateConfig() {
		String returnPage = LinkedIssueConstants.RETURN_REDIRECT;
		// User Acces rights check.
		if (!isUserNotAllowed()) {
			// Retrieve the configuration
			getLNIOConf();
			// retrieve the context to modify
			LnioContextBean contextBean = getContextObjectById(idContext);
			LOG.debug("idContext : " + idContext);
			if (contextBean != null) {
				List listOfFieldsMapping = updateListOfFieldsMapping();
				if (isListOfFieldsMappingIsValide(listOfFieldsMapping)) {
					contextBean.setListOfFieldsMapping(listOfFieldsMapping);
					lnioConfig.getConfiguredContexts().put(idContext,
							contextBean);
					saveLNIOConf();
				} else {
					resultOperation = "action.lnio.config.validation.error";
					returnPage = LinkedIssueConstants.RETURN_UNVALIDATE;
					return returnPage;
				}
			} else {
				resultOperation = "action.lnio.config.error";
				LOG.error("An error occurs, the  contextBean is null ");
				returnPage = LinkedIssueConstants.RETURN_ERROR;
				return returnPage;

			}
			returnPage = LinkedIssueConstants.RETURN_SUCCESS;
		}

		return returnPage;
	}

	/**
	 * Display the valiantys disclaimer.
	 * @return
	 */
	public final String doDisplayDisclaimer(){
		return SUCCESS;
	}
	
	/**
	 * This method is call when the user want to return to the default
	 * configuration. <b>NO FIELD MAPPING CONFIGURED</b><br>
	 * Possible next page name are :<br>
	 * -success : if all action succeed.<br>
	 * -error : if there is error.<br>
	 * -redirect : if user is not allowed.<br>
	 * 
	 * @return the next page name.
	 */
	public final String doReInitConfig() {
		String returnPage = LinkedIssueConstants.RETURN_REDIRECT;
		// User Acces rights check.
		if (!isUserNotAllowed()) {
			// Create a new empty configuration
			try {
				createADefaultCoherentConfig();
				saveLNIOConf();
			} catch (LinkNewIssueOperationException e) {
				LOG.error("An error occurs : " + e.getMessage());
			}
			returnPage = LinkedIssueConstants.RETURN_SUCCESS;
		}
		return returnPage;
	}

	/**
	 * This method check if the user have the good rights to access to this part
	 * of the plugin.<br>
	 * The user must belong to "jira-administrators" group.<br>
	 * <b>Be careful</b>, this method return :<br>
	 * -true : if the user <b>don't have</b> the rights !<br>
	 * -false : if the user <b>have</b> the good rights !
	 * 
	 * @return true if the user is not allowed, else false.
	 */
	private boolean isUserNotAllowed() {
		boolean isUserNotAllowed = true;
		final User currentUser = authenticationContext.getUser();

		if (currentUser != null) {
			final List groupList = currentUser.getGroups();
			if (groupList != null && !groupList.isEmpty()) {
				if (groupList.contains(LinkedIssueConstants.JIRA_ADMIN_GROUP)) {
					isUserNotAllowed = false;
				}
			}
		}

		return isUserNotAllowed;
	}

	/**
	 * Get LNIO Config Object from database.
	 * 
	 * @return LNIO Config object.
	 */
	private LnioFieldsMappingConfiguration getLNIOConf() {
		try {
			if (lnioConfig == null) {
				lnioConfig = loadLNIOConf();
				if (lnioConfig == null) {
					// There are nothing in the database, so initialize new one.
					createADefaultCoherentConfig();
					saveLNIOConf();
				} else {
					// check with there is non incoherence with the xml file and
					// the stored config
					giveCoherentConfig();
				}

			}
		} catch (LinkNewIssueOperationException e) {
			LOG.error("An error occurs : " + e.getMessage());
		}
		return lnioConfig;
	}

	/**
	 * Save SLA configuration into XML format in the database.
	 */
	private void saveLNIOConf() {
		if (lnioConfigManager != null && lnioConfig != null) {
			lnioConfigManager.storeLnioConfigObject(lnioConfig);
			LOG.debug("LNIO New Config saved ");
		} else {
			LOG.error("An error occurs ");
			LOG.debug("lnioConfigManager : " + lnioConfigManager);
			LOG.debug("lnioConfig : " + lnioConfig);
		}
	}

	/**
	 * Load the SLA configuration from the database and create object SLAConfig
	 * to represents the configuration.<br>
	 * 
	 * @return instance of SLAConfig object.
	 */
	private LnioFieldsMappingConfiguration loadLNIOConf() {
		try {
			lnioConfig = lnioConfigManager
					.getLnioFieldsMappingConfigurationObject();
		} catch (LinkNewIssueOperationException e) {
			e.printStackTrace();
			LOG.error("Failed to load LNIO configuration");
		}
		return lnioConfig;
	}

	/**
	 * Create of default coherente configuration.
	 * 
	 * @throws LinkNewIssueOperationException
	 *             : LinkNewIssueOperationException.
	 */
	private void createADefaultCoherentConfig()
			throws LinkNewIssueOperationException {
		lnioConfig = new LnioFieldsMappingConfiguration();
		lnioConfig.setConfiguredContexts(createADefaultHashMapCoherentConfig());
	}

	/**
	 * 
	 * @throws LinkNewIssueOperationException : LinkNewIssueOperationException.
	 */
	private HashMap createADefaultHashMapCoherentConfig()
			throws LinkNewIssueOperationException {
		HashMap lnioContexts = null;
		MappingsItem[] contexts = LinkedIssueContextManager.getInstance()
				.getAllMappings();
		if (contexts != null) {
			lnioContexts = new HashMap(contexts.length);
			LnioContextBean aLnioContextBean = null;
			for (int i = 0; i < contexts.length; i++) {
				Mapping context = contexts[i].getMapping();
				aLnioContextBean = new LnioContextBean();
				aLnioContextBean.setIdContext(String.valueOf(context.getId()));
				aLnioContextBean.setName(context.getName());
				aLnioContextBean.setLabel(context.getLabel());
				aLnioContextBean
						.setLink(LinkedIssueConstants.ACTION_EDIT_FIELDS_MAPPING
								+ context.getId());

				IssueParent issueParent = context.getIssueParent();
				IssueChild issueChild = context.getIssueChild();

				if (issueParent != null) {
					if (issueParent.getIssueParentItemCount() > 0) {
						IssueParentItem[] issueParentItems = issueParent
								.getIssueParentItem();
						String keyParam;
						String valueParam;
						for (int j = 0; j < issueParentItems.length; j++) {
							keyParam = issueParentItems[j].getParam().getKey();
							valueParam = issueParentItems[j].getParam()
									.getValue();
							if (keyParam
									.equals(LinkedIssueConstants.ID_PROJECT)) {
								aLnioContextBean
										.setParentIssueIdProject(valueParam);
							} else if (keyParam
									.equals(LinkedIssueConstants.ID_TYPE)) {
								aLnioContextBean
										.setParentIssueIdType(valueParam);
							}
						}
					}
				}

				if (issueChild != null) {
					if (issueChild.getIssueChildItemCount() > 0) {
						IssueChildItem[] issueChildItems = issueChild
								.getIssueChildItem();
						String keyParam;
						String valueParam;
						for (int j = 0; j < issueChildItems.length; j++) {
							keyParam = issueChildItems[j].getParam().getKey();
							valueParam = issueChildItems[j].getParam()
									.getValue();
							if (keyParam
									.equals(LinkedIssueConstants.ID_PROJECT)) {
								aLnioContextBean
										.setChildIssueIdProject(valueParam);
							} else if (keyParam
									.equals(LinkedIssueConstants.ID_TYPE)) {
								aLnioContextBean
										.setChildIssueIdType(valueParam);
							}
						}
					}
				}
				lnioContexts.put(String.valueOf(context.getId()),
						aLnioContextBean);
			}
		}
		return lnioContexts;
	}

	/**
	 * Check the coherence of the configuration saved and the configuration
	 * file. The saved configuration is updated if the configuration file has
	 * change (addition of context.)
	 * 
	 * @throws LinkNewIssueOperationException
	 *             : LinkNewIssueOperationException.
	 */
	private void giveCoherentConfig() throws LinkNewIssueOperationException {
		if (lnioConfig == null) {
			lnioConfig = new LnioFieldsMappingConfiguration();
		}

		// retrieve contexts form configuration file.
		HashMap lnioContexts = createADefaultHashMapCoherentConfig();

		// retrieve contexts form database.
		HashMap alreadySavedContexts = lnioConfig.getConfiguredContexts();

		if (alreadySavedContexts == null) {
			createADefaultCoherentConfig();
		} else {
			// 
			Iterator contextIte = lnioContexts.values().iterator();
			boolean hasChanged = false;
			while (contextIte.hasNext()) {
				LnioContextBean aLnioContextBean = (LnioContextBean) contextIte
						.next();
				if (!(alreadySavedContexts.containsKey(aLnioContextBean
						.getIdContext()))) {
					alreadySavedContexts.put(aLnioContextBean.getIdContext(),
							aLnioContextBean);
					LOG.debug("alreadySavedContexts add : "
							+ aLnioContextBean.getIdContext() + " - "
							+ aLnioContextBean);
					hasChanged = true;
				}
			}
			if (hasChanged) {
				lnioConfig.setConfiguredContexts(alreadySavedContexts);
				saveLNIOConf();
			}

		}
	}

	/**
	 * Get the collection of the LNIO context.
	 * 
	 * @return collection of agreement time.
	 */
	public final Collection getContexts() {
		return sortCollectionByKey(lnioConfig.getConfiguredContexts().values());
	}


	/**
	 * Retrieve an alphabetic sorted list of context by id.
	 * 
	 * @param collection
	 *            : unsorted collection
	 * @return a sorted list
	 */
	private Collection sortCollectionByKey(final Collection collection) {
		return LnioUtils.bubbleSort(collection);
	}

	/**
	 * return the object representation of a context.
	 * 
	 * @param idCtx
	 *            = context id.
	 * @return return the object representation.
	 */
	public LnioContextBean getContextObjectById(final String idCtx) {
		LnioContextBean contextBean = null;

		if (lnioConfig == null) {
			getLNIOConf();
		}
		HashMap contexts = lnioConfig.getConfiguredContexts();

		if (contexts.containsKey(idCtx)) {
			contextBean = (LnioContextBean) contexts.get(idCtx);
		}
		return contextBean;
	}
	

	/**
	 * Return the fields mapping when it was previously configured.
	 * 
	 * @param idCtx
	 *            : context.
	 * @return list of fields which are mapped.
	 */
	public final List getFieldsMappingActiveByContext(final String idCtx) {
		List fieldsActive = null;
		LnioContextBean contextBean = getContextObjectById(idCtx);
		if (contextBean != null) {
			List fieldsActiveTmp = contextBean.getListOfFieldsMapping();
			if (fieldsActiveTmp != null) {
				fieldsActive = new ArrayList();
				for (int i = 0; i < fieldsActiveTmp.size(); i++) {
					LnioMappingOfFieldBean lnioMappingOfFieldBean = (LnioMappingOfFieldBean) fieldsActiveTmp
							.get(i);
					if (lnioMappingOfFieldBean.isMappingActive()) {
						fieldsActive.add(lnioMappingOfFieldBean
								.getOriginFieldId());
					}

				}

			}
		}
		if (fieldsActive != null) {
			LOG.debug("fieldsActive : " + fieldsActive.size());
		}
		return fieldsActive;
	}

	/**
	 * Return all available fields by a context.
	 * 
	 * @param idCtx
	 *            : context.
	 * @param isDestinationFields
	 *            : flag to know if we retrieve field of origin or destination.
	 * @return return all available fields;
	 */
	public final Collection getFieldsByContext(final String idCtx,
			final boolean isDestinationFields) {
		Collection fields = null;
		LnioContextBean contextBean = getContextObjectById(idCtx);

		if (contextBean != null) {
			try {
				if (isDestinationFields) {
					fields = getListOfAvailableFields(contextBean
							.getChildIssueIdProject(), contextBean
							.getChildIssueIdType());
				} else {
					fields = getListOfAvailableFields(contextBean
							.getParentIssueIdProject(), contextBean
							.getParentIssueIdType());
				}
			} catch (FieldException e) {
				LOG.error("An error occurs : " + e.getMessage());
			}
		}
		fields.removeAll(fieldsUnmappable());
		return fields;
	}

	/**
	 * Return the list of fields.
	 * 
	 * @param issueIdProject
	 * @param issueIdType
	 * @return
	 * @throws FieldException
	 */
	private Collection getListOfAvailableFields(String issueIdProject,
			String issueIdType) throws FieldException {

		// TODO
		FieldManager fieldManager = ManagerFactory.getFieldManager();
		Field d;
		Collection fieldList = fieldManager.getAllAvailableNavigableFields();
		//
		// LOG.debug("parentIssueIdProject : " + issueIdProject);
		// LOG.debug("parentIssueIdType : " + issueIdType);
		//		
		// if(issueIdType!=null && issueIdProject!=null){
		// Project prj = projectManager.getProjectObj(new Long(issueIdProject));
		// LOG.debug("prj : " + prj);
		// IssueTypeSchemeManager itsm =
		// (IssueTypeSchemeManager)ComponentManager.getComponentInstanceOfType(IssueTypeSchemeManager.class);
		// FieldConfigScheme fcs = itsm.getConfigScheme(prj.getGenericValue());
		// LOG.debug("itsm : " + itsm);
		// LOG.debug("fcs : " + fcs.getName());
		// ConfigurableField cf = fcs.getField();
		// LOG.debug("cf : " + cf.getName() + " " + cf.getNameKey());
		// cf.getConfigurationItemTypes();
		// LOG.debug("cf getConfigurationItemTypes: " +
		// cf.getConfigurationItemTypes());
		//			
		//	
		// }
		//		
		// Iterator fieldsIt = fieldList.iterator();
		// while(fieldsIt.hasNext()){
		// LOG.debug(" fieldsIt.next().getClass() : " +
		// fieldsIt.next().getClass());
		// }

		return fieldList;
	}

	/**
	 * Update the list of mapped fields.
	 * 
	 * @return the updated list.
	 */
	private List updateListOfFieldsMapping() {
		List activeMapping = null;

		Enumeration paramatersHttp = request.getParameterNames();
		String keyParam = null;
		String fieldIdOrigin = null;
		String fieldIdDestination = null;
		while (paramatersHttp.hasMoreElements()) {
			keyParam = (String) paramatersHttp.nextElement();
			if (keyParam.indexOf("active_") == 0) {
				if (activeMapping == null) {
					activeMapping = new ArrayList();
				}
				fieldIdOrigin = keyParam.replaceFirst(
						LinkedIssueConstants.PREFIX_ACTIVE, "");
				fieldIdDestination = request
						.getParameter(LinkedIssueConstants.PREFIX_MAP
								+ fieldIdOrigin);

				LnioMappingOfFieldBean aMappingOfField = new LnioMappingOfFieldBean();

				aMappingOfField.setDestinationFieldId(fieldIdDestination);
				aMappingOfField.setOriginFieldId(fieldIdOrigin);
				aMappingOfField.setMappingActive(true);

				LOG.debug(" Mapping added : " + aMappingOfField);
				activeMapping.add(aMappingOfField);
			}
		}
		return activeMapping;
	}

	private boolean isListOfFieldsMappingIsValide(List listOfFieldsMapping) {
		boolean isValide = true;
		List destinationFields = new ArrayList();
		if(listOfFieldsMapping!=null){
		Iterator fieldsMapping = listOfFieldsMapping.iterator();
		while(fieldsMapping.hasNext()){
			String destinationFieldId = ((LnioMappingOfFieldBean)fieldsMapping.next()).getDestinationFieldId();
			if(destinationFields.contains(destinationFieldId)){
					isValide=false;
			}else{
				destinationFields.add(destinationFieldId);
			}
		}
		}
		LOG.debug(" The list of mapped fields is valid : " + isValide);
		return isValide;
	}

	/**
	 * Return the selected field of destination for a specific context and the
	 * specific fields of origin. This method is called for each field of origin
	 * in the fields list.
	 * 
	 * @param idCtx
	 *            : the context.
	 * @param fieldOrg
	 *            : the field.
	 * @return the id of the field.
	 */
	public final String getFieldDestinationSelectedByContextAndFieldOrigin(
			final String idCtx, final String fieldOrg) {
		LnioContextBean contextBean = getContextObjectById(idCtx);
		if (contextBean != null) {
			List fieldsActive = contextBean.getListOfFieldsMapping();
			if (fieldsActive != null) {
				for (int i = 0; i < fieldsActive.size(); i++) {
					LnioMappingOfFieldBean lnioMappingOfFieldBean = (LnioMappingOfFieldBean) fieldsActive
							.get(i);
					if (lnioMappingOfFieldBean.getOriginFieldId()
							.equalsIgnoreCase(fieldOrg)) {
						return lnioMappingOfFieldBean.getDestinationFieldId();
					}
				}
			}
		}
		return null;
	}

	/**
	 * return the unmappable fields. The ids of fields are hard coded.
	 * 
	 * @return a list of fields that we can not map.
	 */
	public final List fieldsUnmappable() {
		List fieldsUnmappable = new ArrayList();

		FieldManager fieldManager = ManagerFactory.getFieldManager();
		for (int i = 0; i < FIELDS_KEYS.length; i++) {
			fieldsUnmappable.add(fieldManager.getField(FIELDS_KEYS[i]));
		}
		return fieldsUnmappable;
	}

	/**
	 * getter.
	 * 
	 * @return the context id.
	 */
	public final String getIdContext() {
		return idContext;
	}

	/**
	 * setter.
	 * 
	 * @param idCtx
	 *            : the context id.
	 */
	public final void setIdContext(final String idCtx) {
		this.idContext = idCtx;
	}

	/**
	 * getter.
	 * 
	 * @return the result of the Operation.
	 */
	public final String getResultOperation() {
		return resultOperation;
	}

	/**
	 * setter.
	 * 
	 * @param resultOp
	 *            : the result of the Operation.
	 */
	public final void setResultOperation(final String resultOp) {
		this.resultOperation = resultOp;
	}
}
