/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3rc1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.valiantys.jira.plugins.createissueandlink.lnioCastor;

/**
 * Class WatcherToAddItem.
 * 
 * @version $Revision$ $Date$
 */
public class WatcherToAddItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _watcher.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.Watcher _watcher;


      //----------------/
     //- Constructors -/
    //----------------/

    public WatcherToAddItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'watcher'.
     * 
     * @return the value of field 'Watcher'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.Watcher getWatcher(
    ) {
        return this._watcher;
    }

    /**
     * Sets the value of field 'watcher'.
     * 
     * @param watcher the value of field 'watcher'.
     */
    public void setWatcher(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.Watcher watcher) {
        this._watcher = watcher;
    }

}
