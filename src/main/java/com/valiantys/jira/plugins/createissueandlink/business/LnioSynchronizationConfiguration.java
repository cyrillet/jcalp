package com.valiantys.jira.plugins.createissueandlink.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class is the representation of the fields mapping global configuration.
 * 
 * @author Maxime
 *@since v 1.4.0
 */
public class LnioSynchronizationConfiguration implements Serializable {

	/**
	 * serial Version UID.
	 */
	private static final long serialVersionUID = -6192266870911651633L;

	/**
	 * A list of LnioContextBean.
	 */
	private List<SynchronizationConfigBean> configuredSynchros;

	/**
	 * default constructor.
	 */
	public LnioSynchronizationConfiguration() {
		configuredSynchros = new ArrayList<SynchronizationConfigBean>();
	}

	public List<SynchronizationConfigBean> getConfiguredSynchros() {
		return configuredSynchros;
	}

	public void setConfiguredSynchros(
			List<SynchronizationConfigBean> configuredSynchros) {
		this.configuredSynchros = configuredSynchros;
	}
	

	
}
