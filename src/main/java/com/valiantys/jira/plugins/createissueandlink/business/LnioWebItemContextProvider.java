package com.valiantys.jira.plugins.createissueandlink.business;

import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueImpl;
import com.atlassian.jira.plugin.issueoperation.IssueOperationModuleDescriptor;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.collect.MapBuilder;
import com.opensymphony.user.User;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.LinkedIssueContextManager;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;

/**
 * This class is used to provide to all issue-operation web-items the context informations they need
 * @author Cl�ment Capiaux
 * @since v1.5.2
 *
 */
public class LnioWebItemContextProvider extends AbstractJiraContextProvider {

	
	/**
	 * Loggeur.
	 */
	private static final Logger LOG = Logger.getLogger(LnioWebItemContextProvider.class);

	/**
	 * permission manager of JIRA.
	 */
	private final PermissionManager permissionManager;

	/**
	 * Atuhentication contxt of JIRA.
	 */
	private final JiraAuthenticationContext jiraAuthenticationContext;

	/**
	 * Descriptor of the jira operation module.
	 */
	private IssueOperationModuleDescriptor descriptor;
	
	/**
	 * Current issue concerned by the webItem
	 */
	private Issue currentIssue;
	
	/**
	 * Default constructor.
	 * 
	 * @param aPermissionManager
	 *            : the permission Manager.
	 * @param aJiraAuthenticationContext
	 *            : the context.
	 */
	public LnioWebItemContextProvider(final PermissionManager aPermissionManager,
			final JiraAuthenticationContext aJiraAuthenticationContext) {
		super();
		this.permissionManager = aPermissionManager;
		this.jiraAuthenticationContext = aJiraAuthenticationContext;
	}
	
	/**
	 * Initialization of the descriptor.
	 * 
	 * @param aDescriptor
	 *            : the descriptor of the issue Operation Module.
	 */
	public final void init(final IssueOperationModuleDescriptor aDescriptor) {
		this.descriptor = aDescriptor;
	}
	

	@Override
	public Map getContextMap(User user, JiraHelper jiraHelper) {
		
		// declarations
		Mapping[] mappingsRanked = null; // La liste des mappings par ordre d�croissant de fid�lit�
		
		/* retrieval of the LinkedIssueContextManager which permit to manage
		 * the configuration defined mappings
		 */
		LinkedIssueContextManager licm = LinkedIssueContextManager.getInstance();
		
		// Matching mappings retrieval
		try {
			mappingsRanked = licm.getRankedMappingsByIssueParent((IssueImpl) jiraHelper.getContextParams().get("issue"));
		} catch (LinkNewIssueOperationException e) {
			LOG.error(e.getMessage());
		}
		
		
		/* construction of the map containing the couples (id;label) of all matching mappings
		 * ranked in descending order of matching level.
		 */
		MapBuilder<String, Object> mp = MapBuilder.newBuilder();
		for (int i = 0; i < mappingsRanked.length; i++) {
			mp.add("mapping" + (i+1), mappingsRanked[i].getId());
			mp.add("mappingLabel" + (i+1) , mappingsRanked[i].getLabel());
		}
		
		return mp.toMap();
	}


}
