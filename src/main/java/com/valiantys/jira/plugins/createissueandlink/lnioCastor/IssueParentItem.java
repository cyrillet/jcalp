/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3rc1</a>, using an XML
 * Schema.
 * $Id$
 */

package com.valiantys.jira.plugins.createissueandlink.lnioCastor;

/**
 * Class IssueParentItem.
 * 
 * @version $Revision$ $Date$
 */
public class IssueParentItem implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _param.
     */
    private com.valiantys.jira.plugins.createissueandlink.lnioCastor.Param _param;


      //----------------/
     //- Constructors -/
    //----------------/

    public IssueParentItem() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/

    /**
     * Returns the value of field 'param'.
     * 
     * @return the value of field 'Param'.
     */
    public com.valiantys.jira.plugins.createissueandlink.lnioCastor.Param getParam(
    ) {
        return this._param;
    }

    /**
     * Sets the value of field 'param'.
     * 
     * @param param the value of field 'param'.
     */
    public void setParam(
            final com.valiantys.jira.plugins.createissueandlink.lnioCastor.Param param) {
        this._param = param;
    }

}
