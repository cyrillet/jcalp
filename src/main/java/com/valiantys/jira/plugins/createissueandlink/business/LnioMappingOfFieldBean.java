package com.valiantys.jira.plugins.createissueandlink.business;

import java.io.Serializable;

/**
 * This class is the representation of a context (mapping) defined in the
 * linkedIssue-context.xml.
 * 
 * @author Maxime
 * @since v 1.4.0
 */
public class LnioMappingOfFieldBean implements Serializable {

	/**
	 * serial Version UID.
	 */
	private static final long serialVersionUID = 8334018842279151661L;

	/**
	 * Name of the field of the origin issue.
	 */
	private String originFieldId;

	/**
	 * Id of the field of the destination issue.
	 */
	private String destinationFieldId;

	/**
	 * flag to know if the mapping is activate.
	 */
	private boolean isMappingActive;

	/**
	 * get the  id the origin field.
	 * @return the id the origin field.
	 */
	public final String getOriginFieldId() {
		return originFieldId;
	}

	/**
	 * set origin field id for this mapping.
	 * @param oriFieldId : id of field of origin.
	 */
	public final void setOriginFieldId(final String oriFieldId) {
		this.originFieldId = oriFieldId;
	}

	/**
	 * get the  id the destination field.
	 * @return the id the destination field.
	*/
	public final String getDestinationFieldId() {
		return destinationFieldId;
	}

	/**
	 * set destination field id for this mapping.
	 * @param destFieldId : id of field of destination.
	 */
	public final void setDestinationFieldId(final String destFieldId) {
		this.destinationFieldId = destFieldId;
	}

	/**
	 * get the active flag on the mapping field.
	 * @return : true if active.
	 */
	public final boolean isMappingActive() {
		return isMappingActive;
	}

	/**
	 * set true to enabled the mapping field.
	 * @param isMapActive : true if active.
	 */
	public final void setMappingActive(final boolean isMapActive) {
		this.isMappingActive = isMapActive;
	}

	/**
	 * {@inheritDoc}
	 */
	public final String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("originFieldId : ");
		sb.append(originFieldId);
		sb.append(" - ");
		sb.append("destinationFieldId : ");
		sb.append(destinationFieldId);
		sb.append(" - ");
		sb.append("isMappingActive : ");
		sb.append(isMappingActive);
		return  sb.toString();
	}
}
